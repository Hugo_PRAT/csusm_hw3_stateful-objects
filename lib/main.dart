import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Homework 3 Stateful Objects',
      theme: ThemeData(
        primarySwatch: Colors.orange,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MainPage(title: 'Homework 3 Stateful Objects'),
    );
  }
}

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  double counter = 0;

  List<Color> colorArray = [Colors.orangeAccent, Colors.redAccent , Colors.blueAccent, Colors.greenAccent, Colors.yellowAccent, Colors.purpleAccent];
  int randomColor = 0;

  Color textColor;
  List<String> textArray = ["Hellow world", "Bonjour le monde", "مرحبا بالعالم", "こんにちは世界", "Привет мир", "Ciao mondo", "Halo Dunia", "Hallo wrâld"];
  int randomTextIndex = 0;

  bool favorite = false;

  @override void initState() {
    super.initState();
    // Timer _startCounter
    Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        Random randomtmp = Random(); 
        randomTextIndex = randomtmp.nextInt(textArray.length); 
      });
    });
  }

  void _changeColor() {
    setState(() { 
      Random randomtmp = Random(); 
      randomColor = randomtmp.nextInt(colorArray.length); 
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          Center(child: Center( child: Column(children: [
            SizedBox(height: 180),
            Text('${textArray[randomTextIndex]}', style: TextStyle(fontSize: 42),),
            SizedBox(height: 40,),
            Text("Discover the hearth of the application\n by tapping a lot in the + button "),
            SizedBox(height: 20,),
            Text( '$counter', style: Theme.of(context).textTheme.headline4,),
            favoriteButton(),
          ],)),),
        ],
      ),
      floatingActionButton: floatButton(),
    );
  }

  Widget floatButton() {
    return RaisedButton(
      onPressed: () {
        setState(() {
          if (counter >= 300) return;
          counter++; 
        });
        _changeColor();
      },
      child: Icon(Icons.add),
      color: colorArray[randomColor],
    );
  }

  Widget favoriteButton() {
    return IconButton(
      icon: favorite == false ? Icon(Icons.favorite_border) : Icon(Icons.favorite),
      iconSize: counter,
      color: Colors.pink,
      onPressed: () {
        setState(() {
          if (favorite == true) 
            favorite = false;
          else
            favorite = true;
        });
      },
    );
  }
}
